Модуль Account для Yii2
=============================

Данное расширение добавляет возможность подключения для разных ролей разных layout'ов.
Так же в данный модуль включена тема bootstrap AdminLTE.

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Add to composer.json:
```json
"emilasp/yii2-accounts": "*"
```
AND
```json
"repositories": [
        {
            "type": "git",
            "url":  "https://bitbucket.org/emilasp/yii2-account.git"
        }
    ]
```

to the require section of your `composer.json` file.

Configuring
--------------------------

Добавляем в конфиг приложения модуль:

```php
'modules' => [
        'account' => [
            'class' => 'emilasp\account\Module',
            'pathTheme' => '@app/themes/default/views/layouts/accounts',
            'is_admin'=>function(){
                    return Yii::$app->user->can('admin');
                },
            'roles'=>function(){
                    return ['admin'];
                },
            'userName'=>function(){
                    return Yii::$app->user->identity->username;
                },
            'userRole'=>function(){
                    return Yii::$app->user->identity->role;
                },
            'accounts'=>[
                'admin'=>[
                    'defaultRoute'=>'/site/pages/',
                    'layout'=>'admin',
                    'icon'=>'@web/images/default/system/accounts/sett.png',
                    'pathCss'=>'',
                    'roles'=>['admin'],
                ],
            ],
        ],
        ...
    ],
```
Указываем
pathTheme - путь до папки с лейаутами для ролей
is_admin - текущий пользователь админ(лямбда функция)
roles - все имеющиеся роли(лямбда функция)
userName - Имя текущего пользователя(лямбда функция)
userRole - роль текущего пользователя(лямбда функция)
accounts - настройки для аккаунтов


2) Добавляем модель в прелоадер приложения:
```php
'bootstrap' => ['account'],
```
3) Правим иконки в конфиге модуля

4) Правим путь в /asset/admin.js

