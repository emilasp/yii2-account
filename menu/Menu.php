<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 04.09.14
 * Time: 14:34
 */

namespace emilasp\account\menu;

use yii;


/**
 * Генерируем меню
 *
 * Class WaManager
 * @package common\components
 */
class Menu {


    public static function generate(){

        $menu = [];

        $systemItems = [];

        if ( \Yii::$app->user->can('p_sitePages')   ) {
            $systemItems[] = ['label' => 'Опции','icon'=>'wrench','url' => '/site/options/', 'items' => [
                ['label' => 'Создать','icon'=>'pencil', 'url' => '/site/options/create'],
                ['label' => 'Список','icon'=>'pencil', 'url' => '/site/options/'],
            ]];
            //$siteItems[] = '<li class="divider"></li>';
        }
        if( count($systemItems) > 0 ) $menu[] = ['label' => 'Система','icon' => 'flash','url' => '#', 'active'=>false, 'items' => $systemItems ];






        $siteItems = [];
        if ( \Yii::$app->user->can('p_sitePages') || \Yii::$app->user->can('p_site') ) {
            $siteItems[] = ['label' => 'Страницы','icon'=>'pencil','url' => '/site/pages/', 'active'=>false, 'items' => [
                ['label' => 'Создать','icon'=>'pencil', 'url' => '/site/pages/create'],
                ['label' => 'Список','icon'=>'pencil', 'url' => '/site/pages/'],
            ]];
            //$siteItems[] = '<li class="divider"></li>';
        }
        if ( \Yii::$app->user->can('p_sitePosts') || \Yii::$app->user->can('p_site')  ) {
            $siteItems[] = ['label' => 'Статьи','icon'=>'pencil','url' => '/site/posts/', 'items' => [
                ['label' => 'Создать','icon'=>'pencil', 'url' => '/site/posts/create'],
                ['label' => 'Список','icon'=>'pencil', 'url' => '/site/posts/'],
            ]];
        }
        if ( \Yii::$app->user->can('p_sitePosts') || \Yii::$app->user->can('p_site')  ) {
            $siteItems[] = ['label' => 'Новости','icon'=>'pencil','url' => '/site/news/', 'items' => [
                ['label' => 'Создать','icon'=>'pencil', 'url' => '/site/news/create'],
                ['label' => 'Список','icon'=>'pencil', 'url' => '/site/news/'],
            ]];
        }
        if ( \Yii::$app->user->can('p_sitePosts') || \Yii::$app->user->can('p_site')  ) {
            $siteItems[] = ['label' => 'Файлы','icon'=>'pencil','url' => '/site/files/', 'items' => [
                ['label' => 'Создать','icon'=>'pencil', 'url' => '/site/files/create'],
                ['label' => 'Список','icon'=>'pencil', 'url' => '/site/files/'],
            ]];
        }

        if ( \Yii::$app->user->can('p_siteNews') || \Yii::$app->user->can('p_site')  ) {
            $siteItems[] = ['label' => 'Новости','icon'=>'pencil','url' => '#', 'items' => [
                ['label' => 'Создать','icon'=>'pencil', 'url' => '/'],
                ['label' => 'Список','icon'=>'pencil', 'url' => '/'],
            ]];
            //$siteItems[] = '<li class="divider"></li>';
        }


        if ( \Yii::$app->user->can('p_siteNews') || \Yii::$app->user->can('p_site')  ) {
            $siteItems[] = ['label' => 'Категории','icon'=>'pencil','url' => '/category/category/index', 'items' => [
                ['label' => 'Категории','icon'=>'pencil','url' => '/category/category/index', 'items' => [
                    ['label' => 'Создать','icon'=>'pencil', 'url' => '/category/category/create'],
                    ['label' => 'Список','icon'=>'pencil', 'url' => '/category/category/index'],
                ]],
                ['label' => 'Категории link','icon'=>'pencil','url' => '/category/cat-model/index', 'items' => [
                    ['label' => 'Создать','icon'=>'pencil', 'url' => '/category/cat-model/create'],
                    ['label' => 'Список','icon'=>'pencil', 'url' => '/category/cat-model/index'],
                ]],
            ]];

            //$siteItems[] = '<li class="divider"></li>';

        }

        if ( \Yii::$app->user->can('p_siteNews') || \Yii::$app->user->can('p_site')  ) {
            $siteItems[] = ['label' => 'Теги','icon'=>'pencil','url' => '/tags/tags/index', 'items' => [
                ['label' => 'Теги','icon'=>'pencil','url' => '/tags/tags/index', 'items' => [
                    ['label' => 'Создать','icon'=>'pencil', 'url' => '/tags/tags/create'],
                    ['label' => 'Список','icon'=>'pencil', 'url' => '/tags/tags/index'],
                ]],
                ['label' => 'Теги link','icon'=>'pencil','url' => '/tags/tags-model/index', 'items' => [
                    ['label' => 'Создать','icon'=>'pencil', 'url' => '/tags/tags-model/create'],
                    ['label' => 'Список','icon'=>'pencil', 'url' => '/tags/tags-model/index'],
                ]],
            ]];

            //$siteItems[] = '<li class="divider"></li>';

        }

        if ( \Yii::$app->user->can('p_siteNews') || \Yii::$app->user->can('p_site')  ) {
            $siteItems[] = ['label' => 'Комментарии','icon'=>'pencil','url' => '#', 'items' => [
                ['label' => 'Создать','icon'=>'pencil', 'url' => '/comment/comment/create'],
                ['label' => 'Список','icon'=>'pencil', 'url' => '/comment/comment/index']
            ]];
            //$siteItems[] = '<li class="divider"></li>';
        }
        if ( \Yii::$app->user->can('p_siteOptions') || \Yii::$app->user->can('p_site')  ) {
            //$siteItems[]  = '<li class="divider"></li>';
            $siteItems[]  = ['label' => 'Настройки','icon'=>'pencil', 'url' => '/'];
        }
        if( count($siteItems) > 0 ) $menu[] = ['label' => 'Сайт','icon' => 'fire','url' => '#', 'active'=>false, 'items' => $siteItems ];


        /*
         * Татуировки
         */
        $tattooItems = [];

        if ( \Yii::$app->user->can('p_sitePages')   ) {
            $tattooItems[] = ['label' => 'Галереи','icon'=>'wrench','url' => '/tattoo/gallery', 'items' => [
                ['label' => 'Создать','icon'=>'pencil', 'url' => '/tattoo/gallery/create'],
                ['label' => 'Список','icon'=>'pencil', 'url' => '/tattoo/gallery/index'],
            ]];
            $tattooItems[] = ['label' => 'Татуировки','icon'=>'wrench','url' => '/tattoo/tattoo', 'items' => [
                ['label' => 'Создать','icon'=>'pencil', 'url' => '/tattoo/tattoo/create'],
                ['label' => 'Список','icon'=>'pencil', 'url' => '/tattoo/tattoo/index'],
            ]];
            //$siteItems[] = '<li class="divider"></li>';
        }
        if( count($tattooItems) > 0 ) $menu[] = ['label' => 'Тату','icon' => 'star','url' => '#', 'active'=>false, 'items' => $tattooItems ];





        /**
         * Users
         */

        $userItems = [];
        if ( \Yii::$app->user->can('admin') ) {
            $userItems = [
                ['label' => 'Пользователи','icon'=>'pencil','url' => '/user/user/', 'items' => [
                    ['label' => 'Создать','icon'=>'pencil', 'url' => '/user/user/create'],
                    ['label' => 'Список','icon'=>'pencil', 'url' => '/user/user'],
                ]],
                ['label' => 'Права','icon'=>'pencil','url' => '#', 'items' => [
                    ['label' => 'Роли','icon'=>'pencil', 'url' => '/admin/role', 'items' => [
                        ['label' => 'Создать','icon'=>'pencil', 'url' => '/user/user/create'],
                        ['label' => 'Список','icon'=>'pencil', 'url' => '/user/user'],
                    ]],
                    ['label' => 'Разрешения','icon'=>'pencil', 'url' => '/admin/permission', 'items' => [
                        ['label' => 'Создать','icon'=>'pencil', 'url' => '/user/user/create'],
                        ['label' => 'Список','icon'=>'pencil', 'url' => '/user/user'],
                    ]],
                    ['label' => 'Пользователи','icon'=>'pencil', 'url' => '/admin/assigment', 'items' => [
                        ['label' => 'Создать','icon'=>'pencil', 'url' => '/user/user/create'],
                        ['label' => 'Список','icon'=>'pencil', 'url' => '/user/user'],
                    ]],
                    ['label' => 'route','icon'=>'pencil', 'url' => '/admin/route', 'items' => [
                        ['label' => 'Создать','icon'=>'pencil', 'url' => '/user/user/create'],
                        ['label' => 'Список','icon'=>'pencil', 'url' => '/user/user'],
                    ]],
                    ['label' => 'Правила','icon'=>'pencil', 'url' => '/admin/rule', 'items' => [
                        ['label' => 'Создать','icon'=>'pencil', 'url' => '/user/user/create'],
                        ['label' => 'Список','icon'=>'pencil', 'url' => '/user/user'],
                    ]],
                ]],
                //'<li class="divider"></li>',
                ['label' => 'Настройки','icon'=>'pencil', 'url' => '#']
            ];
        }

        if( count($userItems) > 0 ) $menu[] = ['label' => 'Пользователи','icon' => 'user','url' => '#', 'active'=>false, 'items' => $userItems ];

        /**
         * Users
         */

        $adminItems = [];
        if ( \Yii::$app->user->can('admin') ) {
            $adminItems = [
                ['label' => 'Func','icon'=>'pencil', 'url' => '/site/admin/adminimize']
            ];
        }

        if( count($adminItems) > 0 ) $menu[] = ['label' => 'Adminimize','icon' => 'user','url' => '#', 'active'=>false, 'items' => $adminItems ];



        /**
         * City and Country
         */

        $userItems = [];
        if ( \Yii::$app->user->can('admin') ) {
            $userItems = [
                ['label' => 'Города','icon'=>'pencil', 'url' => '/geoapp/city'],
                ['label' => 'Регионы','icon'=>'pencil', 'url' => '/geoapp/region'],
                ['label' => 'Страны','icon'=>'pencil', 'url' => '/geoapp/country'],
            ];
        }

        if( count($userItems) > 0 ) $menu[] = ['label' => 'ГЕО','icon' => 'map-marker','url' => '', 'active'=>false, 'items' => $userItems ];

        /**
         * All
         */

        $isG = \Yii::$app->user->isGuest;

        if($isG)
            $menu[] = ['label' => 'Login','icon'=>'pencil', 'url' => '/user/default/login'];

        if(!$isG){
            $menu[] = [
                'label' => 'Logout (' . \Yii::$app->user->identity->username . ')',//
                'icon'=>'pencil',
                'url' => '/user/default/logout',
                //'linkOptions' => ['data-method' => 'post'],
                // 'visible'=>!\Yii::$app->user->isGuest
            ];
        }
        return $menu;

    }

}

/*$contact = new waContact(1);
$contact->addToCategory(12);
$contact->addToCategory('blog');*/