<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 04.09.14
 * Time: 14:34
 */

namespace emilasp\account\menu;

use emilasp\front\user\models\FrontUser;
use kartik\icons\Icon;
use yii\helpers\Url;
use yii;


/**
 * Генерируем меню
 *
 * Class WaManager
 * @package common\components
 */
class FrontMenu {


    public static function generate(){

        $menu = [];

        $userRole = false;

        $isGuest  = Yii::$app->user->isGuest;
        if( !$isGuest ) $userRole = Yii::$app->user->identity->role;

        /**
         * For Auth users
         */
        /*$authItems = [];
        if ( $userRole==FrontUser::ROLE_USER  ) {
            $authItems[] = ['label' => 'Опции','icon'=>'wrench','url' => '/site/options/', 'items' => [
                ['label' => 'Создать','icon'=>'pencil', 'url' => '/site/options/create'],
                ['label' => 'Список','icon'=>'pencil', 'url' => '/site/options/'],
            ]];
            //$siteItems[] = '<li class="divider"></li>';
        }
        if( count($authItems) > 0 ) $menu[] = ['label' => 'Система','icon' => 'flash','url' => '#', 'active'=>false, 'items' => $authItems ];*/


        /**
         * All
         */

        $masters = [];

        /*$mastersItems = [
            ['label' => '','url' => ''],
            '<li class="divider"></li>',
            ['label' => 'Категории тату','url' => '/site/options/', 'items' => [
                ['label' => 'Создать', 'url' => '/site/options/create'],
                ['label' => 'Список', 'url' => '/site/options/'],
            ]],
            '<li class="divider"></li>',
            ['label' => 'Подборки','url' => '/site/options/'],
        ];*/
            //$siteItems[] = '<li class="divider"></li>';

        //$menu[] = ['label' => Icon::show('group', ['class' => 'fa-1x'], Icon::FA).' ' . 'Мастера/Салоны','url' => Url::toRoute('/site/options/'), 'active'=>false ];
        $menu[] = '<li class="divider"></li>';
        $menu[] = ['label' => Icon::show('list-ul', ['class' => 'fa-1x'], Icon::FA).' ' . 'Категории тату','url' => Url::toRoute('/tattoo/category/index'), 'active'=>false ];;
        $menu[] = ['label' => Icon::show('graduation-cap', ['class' => 'fa-1x'], Icon::FA).' ' . 'Публикации','icon' => 'flash','url' => '#', 'active'=>false, 'items' => [
            ['label' => 'Новости', 'url' => Url::toRoute('/site/news/index')],
            ['label' => 'Статьи', 'url' => Url::toRoute('/site/posts/index')],
            ['label' => 'Галереи', 'url' => Url::toRoute('/tattoo/gallery/index')],
            //['label' => 'Подборки', 'url' => Url::toRoute('/tattoo/gallery/index')],
            ]
        ];



        if($isGuest){
            $menu[] = ['label' => Icon::show('camera', ['class' => 'fa-1x'], Icon::FA).' ' . 'Добавить тату', 'url' => '#','linkOptions' => ['id' => 'getLoginForm'], 'active'=>false ];
        }else{
            $menu[] = ['label' => Icon::show('camera', ['class' => 'fa-1x'], Icon::FA).' ' . 'Добавить тату', 'url' => Url::toRoute('/user/tattoo/create'), 'active'=>false ];
        }


        //$user = Yii::$app->user;
        //$user = Yii::$app->user->logout();
        /*
         * User menu
         */
        $menuUser = [];

        if($isGuest){
            $menuUser[] = ['label' => Icon::show('check-square-o', ['class' => 'fa-1x'], Icon::FA).' ' . Yii::t('site','Registration'), 'url' => '#','linkOptions' => ['id' => 'getRegistrationForm'],];
            $menuUser[] = ['label' => Icon::show('user', ['class' => 'fa-1x'], Icon::FA).' ' . Yii::t('site','Login'), 'url' => '#','linkOptions' => ['id' => 'getLoginForm']];

        }

        if(!$isGuest){
            $menuUser[] = [
                'label' => Icon::show('cogs', ['class' => 'fa-1x'], Icon::FA).' <span class="menuUsername">' . ((\Yii::$app->user->identity->profiles!=null)?\Yii::$app->user->identity->profiles->first_name:'') . '<span class="caret"></span></span> ',//
                'icon'=>'pencil',
                'url' => '#',
                'linkOptions' => ['id' => 'menuPopupUser'],
                //'options' => ['class'=>'nav navbar-nav navbar-right']
                //'linkOptions' => ['data-method' => 'post'],
                // 'visible'=>!\Yii::$app->user->isGuest
            ];
        }
        return [
            ['options'=>['class' => 'navbar-nav'],'items'=>$menu],
            ['options'=>['class' => 'navbar-nav navbar-right'],'items'=>$menuUser]
        ];


    }

}

/*$contact = new waContact(1);
$contact->addToCategory(12);
$contact->addToCategory('blog');*/