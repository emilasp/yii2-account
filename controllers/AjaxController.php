<?php
//test git 3
namespace emilasp\account\controllers;


use Yii;

use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;


/**
 * SOrderController implements the CRUD actions for STool model.
 */
class AjaxController extends Controller
{

    private $allowParams = [
        'colorTheme',
        'sideBarVisible'
    ];


    public function behaviors()
    {
        return [

        ];
    }

    /**
     * Возвращает список услуг
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionSetparam()  {

        $name = Yii::$app->request->post('name');
        $value = Yii::$app->request->post('value');

        if( !in_array($name, $this->allowParams)) {
            throw new NotFoundHttpException('The requested page does not exist.');
            return;
        }

        $cookies = Yii::$app->response->cookies;

// add a new cookie to the response to be sent
        $cookies->add(new \yii\web\Cookie([
            'name' => $name,
            'value' => $value,
        ]));
    }




}
