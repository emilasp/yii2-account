<?php

namespace emilasp\account;


use emilasp\core\CoreModule;
use Yii;

class Module extends CoreModule
{

    public $DEBUG = false;

    public $controllerNamespace = 'emilasp\account\controllers';


    public $defaultController = '';
    public $layout = '';


    public $accounts = [];

    /**
     * Имя пользователя
     * @var bool|callable
     */
    public $userName;

    /**
     * Доп инфо юзер
     * @var bool|callable
     */
    public $userRole;


    /**
     * Текущий пользователь - админ
     * @var bool|callable
     */
    public $is_admin;

    /**
     * Текущий пользователь - роли(группы)
     * @var array|callable
     */
    public $roles;

    /**
     * Настройки для аккаунтов
     * @var array
     */
    private $accSettings = [];

    /**
     * Аккаунт текущего пользователя
     * @var bool|string
     */
    public $account = false;

    /**
     * Путь до папки с лейаутами аккаунтов
     * @var string
     */
    public $pathTheme = '';

    /**
     * Параметры модуля
     * @var array
     */
    public $module;

    public $defaultMenu = 'Menu';
    public $menu = [];
    public $generateMenu = true;


    /**
     * Initializes
     */
    public function init()
    {
        parent::init();

        //if( is_null($this->module) ) $this->module =  \Yii::$app->modules['account'];

        $this->accSettings =  $this->accounts;

        $is_admin = $this->is_admin;
        $this->is_admin =  $is_admin();

        $roles = $this->roles;
        $this->roles =  $roles();

        if( !$this->DEBUG && $this->is_admin || count($this->roles)==0 && $this->is_admin  ) {
            $this->account = 'admin';
            $this->roles = 'admin';
        }

        $userName = $this->userName;
        $this->userName =  $userName();

        $userRole = $this->userRole;
        $this->userRole =  $userRole();

        if( !$this->account ){
            $this->setAccount();
        }

        $this->settingsToApp();

        if($this->generateMenu)
            $this->menu = call_user_func(['\emilasp\account\menu\\'.$this->defaultMenu, 'generate']);

    }


    /**
     * Устнавливаем текущий аккаунт - первая попавшаяся в переборе роль(группа)
     */
    private function setAccount(){

        foreach( $this->accSettings as $accName=>$account ){
            if($this->account) break;
            foreach( $account['roles'] as $role ){
                foreach( $this->roles as $_role){
                    if( $role==$_role ) {
                        $this->account = $accName;
                        break;
                    }
                }

            }
        }
    }

    /**
     * Устанавливаем настройки приложения в соответствии с настройками аккаунта
     */
    private function settingsToApp(){

        if( !isset($this->accSettings[$this->account]['layout']) || $this->accSettings[$this->account]['layout']=='' ) $this->accSettings[$this->account]['layout'] = 'main';


        Yii::$app->layout = $this->pathTheme.'/'.$this->accSettings[$this->account]['layout'];
        Yii::$app->defaultRoute = $this->accSettings[$this->account]['defaultRoute'];

    }


}
